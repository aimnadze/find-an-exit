function Animations () {
    return {
        stand_east: [
            [0, 0],
        ],
        stand_west: [
            [0, 1],
        ],
        move_east: [
            [1, 0],
            [2, 0],
            [3, 0],
            [4, 0],
        ],
        move_west: [
            [1, 1],
            [2, 1],
            [3, 1],
            [4, 1],
        ],
        move_south_east: [
            [5, 0],
            [6, 0],
            [7, 0],
            [8, 0],
        ],
        move_south_west: [
            [5, 1],
            [6, 1],
            [7, 1],
            [8, 1],
        ],
        move_north: [
            [9, 0],
            [10, 0],
            [11, 0],
            [12, 0],
        ],
    }
}
