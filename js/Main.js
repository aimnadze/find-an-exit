function Main () {
    LoadTile('tiles.png', function (tiles) {

        function animate (name) {
            if (name === current_animation) return
            current_animation = name
            frame = -1
            frames = animations[name]
            clearTimeout(timeout)
        }

        function draw () {
            var [x, y] = frames[frame]
            context.clearRect(0, 0, canvas.width, canvas.height)
            ;(function () {
                for (var y = 0; y < size + 1; y++) {
                    for (var x = 0; x < size + 1; x++) {
                        context.drawImage(tiles[1][9], center[0] + (x - 1) * scale, center[1] + (y - 1) * scale)
                    }
                }
            })()
            context.drawImage(tiles[y][x], scale, scale)
            timeout = setTimeout(function () {
                if (pressed.ArrowLeft) {
                    center[0] = (center[0] + 1) % scale
                    current_side = 'west'
                    animate('move_west')
                } else if (pressed.ArrowRight) {
                    center[0] = (center[0] - 1 + scale) % scale
                    current_side = 'east'
                    animate('move_east')
                } else if (pressed.ArrowDown) {
                    center[1] = (center[1] - 1 + scale) % scale
                    animate('move_south_' + current_side)
                } else if (pressed.ArrowUp) {
                    center[1] = (center[1] + 1) % scale
                    animate('move_north')
                } else {
                    animate(current_side === 'west' ? 'stand_west' : 'stand_east')
                }
                frame = (frame + 1) % frames.length
                draw()
            }, 100)
        }

        var current_side = 'east'
        var current_animation = 'stand_east'
        var center = [0, 0]

        var size = 3
        var scale = 10

        var canvas = document.createElement('canvas')
        canvas.width = canvas.height = size * scale
        canvas.style.imageRendering = 'crisp-edges'
        if (canvas.style.imageRendering !== 'crisp-edges') {
            canvas.style.imageRendering = 'pixelated'
        }
        canvas.style.position = 'absolute'
        canvas.style.top = canvas.style.right = canvas.style.bottom = canvas.style.left = '0'
        canvas.style.margin = 'auto'
        canvas.style.width = canvas.style.height = size * scale * 10 + 'px'

        var context = canvas.getContext('2d')

        var animations = Animations()
        var timeout
        var frame = 0
        var frames = animations[current_animation]
        draw()

        var body = document.body
        body.appendChild(canvas)

        var pressed = Object.create(null)
        addEventListener('keydown', function (e) {
            console.log(e.key)
            pressed[e.key] = true
        })
        addEventListener('keyup', function (e) {
            delete pressed[e.key]
        })

    })
}
