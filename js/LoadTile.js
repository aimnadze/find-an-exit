function LoadTile (name, callback) {

    var image = new Image
    image.src = name
    image.onload = function () {
        var width = (image.width - 1) / 11
        var height = (image.height - 1) / 11
        var tiles = []
        for (var y = 0; y < height; y++) {
            tiles[y] = []
            for (var x = 0; x < width; x++) {
                var canvas = document.createElement('canvas')
                canvas.width = canvas.height = 10
                canvas.getContext('2d').drawImage(image, -x * 11 - 1, -y * 11 - 1)
                tiles[y][x] = canvas
            }
        }
        callback(tiles)
    }

}
